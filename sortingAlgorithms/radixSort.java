package sortingAlgorithms;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Implementing Radix Sort Algorithm
 * @author Abdulrhman Alkhodiry
 *
 * File sortingAlgorithms/radixSort.java
 * was created on Nov 28, 2011 | 5:58:11 PM
 */
public class radixSort {

    /**
     * To hold the number array to be printed in toString function
     */
    String intArray[], maxNum;

    /**
     * Radix Sort (Sorting array A[size])
     * Create all of the bins.
     * From the least significant digit to the most significant digit
     * {
     *  For each element (from the first to the last)
     *  {
     *    Isolate the value of the significant digit. 
     *    Store the element in the bin with the matching significant digit value. 
     *  }
     * For each bin (from the first to the last)
     * {
     * Retrieve all of the elements and store them back into the array.
     * }
     * }
     * Destroy all of the bins.
     * @param _intArray  the array to be sorted
     */
    public radixSort(int _intArray[]) {
        this.intArray = new String[_intArray.length]; // init the main array
        this.fixNumers(_intArray); // adding the leading zeroes
        LinkedList<String>[] ll = (LinkedList<String>[]) new LinkedList[10]; // creating a linked list to hold the numbers 0 to 9
        ArrayList<String> _numbers = new ArrayList<String>(); // Creating an array list to get the contant of the linked list in order
        for (int i = 0; i < ll.length; i++) {  // looping the linked list array
            ll[i] = new LinkedList(); // init the linked list with a linked list
        }
        for (int j = this.maxNum.length(); j > 0; j--) { // getting the least significant number
            for (int i = 0; i < _intArray.length; i++) { // looping the array 
                int x = this.getNumAt(this.intArray[i], j); // getting the number at pos. i
                ll[x].add(this.intArray[i]); // adding the number to the linked list array to its matching number
            }

            for (int i = 0; i < ll.length; i++) { // looping the linked list array
                _numbers.addAll(ll[i]); // adding the number to the Array list
                ll[i].clear();  // clearing the linked list
            }
            _numbers.toArray(this.intArray); // setting the contant of the array list to the main list
            _numbers.clear(); // clearing the array list
        }
    }

    /**
     * making the number as the same length
     * @param _numberArray the number array
     */
    private void fixNumers(int _numberArray[]) {
        this.maxNum = String.valueOf(getMaxNum(_numberArray)); // getting the max number
        for (int i = 0; i < _numberArray.length; i++) { // looping though the array
            String _theNum = String.valueOf(_numberArray[i]); // getting the number as a String
            this.intArray[i] = this.addZeroes(_theNum, this.maxNum.length() - _theNum.length()); // adding leading zeroes
        }
    }

    /**
     * Getting the max number
     * @param _A the array that want to find it max number
     * @return the max number in the array
     */
    private int getMaxNum(int _A[]) {
        int _maxNum = 0; // setting the max to zero
        for (int i = 0; i < _A.length; i++) { // looping through the array
            if (_A[i] > _maxNum) { // comparing the current number to the max
                _maxNum = _A[i]; // if the current number is larger the the max, setting the max number to it
            }
        }
        return _maxNum; // returnint the max number
    }

    /**
     * To get the number in the location specified
     * @param num the full number as a String to keep the leading zeroes
     * @param location the location to get the number of
     * @return the number as integer
     */
    private int getNumAt(String num, int location) {
        char nu = num.charAt(location - 1); // geting the number at the location specified
        return Integer.parseInt(nu + ""); // returning the number 
    }

    /**
     * Adding leading zeroes to the number
     * @param num the number as String
     * @param numOfZeroes number of zeroes to be added
     * @return the number as a String to keep the leading zeroes
     */
    private String addZeroes(String num, int numOfZeroes) {
        for (int i = 0; i < numOfZeroes; i++) { // looping to add zeroes
            num = "0" + num; // adding zeroes
        }
        return num;
    }

    /**
     * Printing the array as a string
     * @return The sorted array as String
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(""); // creating the String Builder to contain the array

        for (int i = 0; i < this.intArray.length; i++) { // looping through the array
            sb.append(Integer.parseInt(this.intArray[i])); // appending the number to the string builder
            sb.append(","); // appending the "," to the string builder
        }

        return sb.toString(); // returning the string builder as a string
    }
}
