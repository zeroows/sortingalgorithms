package sortingAlgorithms;

/**
 *
 * @author zeroows
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int A[] = {1, 1000, 554, 23, 448, 85, 23, 23, 52, 493, 12, 715, 710, 195, 37, 582, 340, 385, 1000, 3421, 302, 123, 23, 109};
        CountingSort cs = new CountingSort(A);
        radixSort rs = new radixSort(A);
        System.out.printf("this array \n %s \nis sorted by Radix Sort Algorithm\n",rs);
        System.out.println();
        System.out.printf("this array \n %s \nis sorted by Counting Sort Algorithm\n",cs);
    }
}
