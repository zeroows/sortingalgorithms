package sortingAlgorithms;

/**
 * Implementing Counting Sort Algorithm
 * @author Abdulrhman Alkhodiry
 *
 * File sortingAlgorithms/CountingSort.java
 * was created on Nov 28, 2011 | 8:10:35 PM
 */
public class CountingSort {
    /**
     * To hold the number array to be printed in toString function
     */
    private int intArray[];

    /**
     * to do the Counting Sort
     * @param A the array to be sorted
     */
    public CountingSort(int A[]) {
        int _elemn = getMaxNum(A);  // get the k the max number in the array
        int _numArray[] = new int[_elemn + 1]; // creating a new array to be used in counting

        /**
         * initializing the array
         */
        for (int i = 0; i < _numArray.length; i++) {
            _numArray[i] = 0;
        }

        /**
         * counting how many is the number occurs in the array
         */
        for (int j = 0; j < A.length; j++) {
            _numArray[A[j]]++;
        }
        
        /**
         * determining how many elements are less or equal to i by keeping running sum of the array _numArray
         */
        for (int i = 1; i < _numArray.length; i++) {
            _numArray[i] = _numArray[i] + _numArray[i - 1];
        }
        /**
         * Making a new array with the size of A
         */
        int _localArray[] = new int[A.length];
        
        /**
         * setting the number from the array wanted to be sorted in its place
         * and decreasing the occurrence of it by 1
         */
        for (int j = A.length - 1; j >= 0; j--) {
            _localArray[_numArray[A[j]]-1] = A[j];
            _numArray[A[j]]--;
        }
        /**
         * setting the sorted array to the main array to be printed
         */
        this.intArray = _localArray;
    }

    /**
     * Getting the max number
     * @param _A the array that want to find it max number
     * @return the max number in the array
     */
    private int getMaxNum(int _A[]) {
        int _maxNum = 0; // setting the max to zero
        for (int i = 0; i < _A.length; i++) { // looping through the array
            if (_A[i] > _maxNum) { // comparing the current number to the max
                _maxNum = _A[i]; // if the current number is larger the the max, setting the max number to it
            }
        }
        return _maxNum; // returnint the max number
    }

        
    /**
     * Printing the array as a string
     * @return The sorted array as String
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(""); // creating the String Builder to contain the array

        for (int i = 0; i < this.intArray.length; i++) { // looping through the array
            sb.append(this.intArray[i]); // appending the number to the string builder
            sb.append(","); // appending the "," to the string builder
        }

        return sb.toString(); // returning the string builder as a string
    }
}
